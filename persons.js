const members = [
  {
    id:1,
    name: 'John Bee',
    email: 'john@gmail.com',
    status: 'active'
  },
  {
    id:2,
    name: 'Adam Dewee',
    email: 'adam@gmail.com',
    status: 'inactive'
  },
  {
    id:3,
    name: 'Mark Sandman',
    email: 'mark@gmail.com',
    status: 'active'
  }
];

module.exports = members;