const express = require('express');
const path = require('path');
const logger = require('./middleware/logger');


const app = express();

// middleware

// body parser
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// use is method to use middleware and SET static folder, in this case 'public'

app.use(express.static(path.join(__dirname,'public')));

app.use('/api/members', require('./routes/api/members'));

const port = process.env.port || 5000;

app.listen(port, ()=>
  console.log(`server running at port: http://localhost:${port}`)
);

