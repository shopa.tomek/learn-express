const express = require('express');
const uuid = require('uuid');
const router = express.Router();
const members = require('../../persons'); 

// get all members

router.get('/', (req, res)=>
  res.json(members));

// GET method/ get a  single member

router.get('/:id', (req, res)=> {
  const found = members.some(member => member.id === parseInt(req.params.id));

  if (found){
  res.json(members.filter(member => member.id === parseInt(req.params.id)));
  } else {
    res.status(400).json({ message: `there's any member with id of ${req.params.id}` });
  }
});

// POST method/ create a member

router.post('/', (req, res)=>{
  const newMember = {
    id:uuid.v4(),
    name: req.body.name,
    email: req.body.email,
    status: 'active'
  }

  if(!newMember.name || !newMember.email) {
    res.status(400).json({message: "type name and email"});
  }
  members.push(newMember);
  res.json(members);

});


module.exports = router;